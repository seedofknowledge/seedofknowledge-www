# Generated by Django 2.1.1 on 2018-09-30 12:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0007_auto_20180929_1829'),
    ]

    operations = [
        migrations.AlterField(
            model_name='enrollmentapplication',
            name='preferred_days',
            field=models.CharField(max_length=80),
        ),
        migrations.AlterField(
            model_name='enrollmentapplication',
            name='preferred_time',
            field=models.CharField(max_length=80),
        ),
    ]
