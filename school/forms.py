from django.forms import ModelForm, HiddenInput
from .models import Classroom

class ClassroomForm(ModelForm):
    class Meta:
        model = Classroom
        fields = ('label', 'school', 'capacity')
        widgets = {'school': HiddenInput}
