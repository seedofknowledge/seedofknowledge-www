from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator

class CourseCategory(models.Model):
    name = models.CharField(max_length=60, null=False, blank=False)
    parent = models.ForeignKey('self', default=None, null=True, blank=True, on_delete=models.CASCADE)
    description = models.TextField()

    def __str__(self):
        return self.fully_qualified_name()

    def fully_qualified_name(self):
        if self.parent:
            return self.parent.name + " / " + self.name
        else:
            return self.name

    def save(self, *args, **kwargs):
        if self.name == '':
            raise ValidationError('Category name cannot be empty')

        qs = CourseCategory.objects.filter(name=self.name)
        
        if self.parent:
            qs = qs.filter(parent=self.parent)
        else:
            qs = qs.filter(parent=None)

        if self.id:
            qs = qs.exclude(pk=self.pk)

        if qs.exists():
            raise ValidationError('Duplicate course category')

        super().save(*args, **kwargs)

class Course(models.Model):
    title = models.CharField(max_length=100)
    level = models.IntegerField(validators=[MinValueValidator(1)])
    category = models.ForeignKey(CourseCategory, related_name='courses', on_delete=models.CASCADE)
    short_description = models.TextField(blank=True, default='')
    description = models.TextField()
    lesson_count = models.IntegerField()
    active = models.BooleanField(default=True)
    price = models.IntegerField(default=0)

    def __str__(self):
        return "{} {}".format(self.title, self.level)

    def save(self, *args, **kwargs):
        if self.title is None:
            raise ValidationError("Course title cannot be null")
        elif self.title.strip() == '':
            raise ValidationError("Course title cannot be empty")

        if not self.level is None and self.level < 1:
            raise ValidationError("Level must be greater than 0")

        super().save(*args, **kwargs)

