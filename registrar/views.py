from django.shortcuts import render
from django.db.models import Count
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.views.generic import FormView, ListView
from students.models import *
from course.models import Course
from school.models import Classroom
from .forms import StudyGroupForm

class StudyGroupCreate(UserPassesTestMixin, FormView):
    #model = StudyGroup
    #fields = ['course', 'classroom', 'teacher', 'comment']
    template_name = 'students/studygroup_form.html'
    form_class = StudyGroupForm
    context_object_name = 'study_group'
    success_url = reverse_lazy('study-group-list')

    def test_func(self):
        return self.request.user.profile.registrar

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'].fields['course'].queryset = Course.objects.filter(active=True)
        return context

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
        
        #self.day = form.day
        #self.start_time = datetime.time(hour=form.start_time)
        #self.end_time = datetime.time(hour=form.start_time + 1, minute=45)
        #return super().form_valid(form)

def unassigned_students_by_course(request):
    # XXX: there should be a much better way to do this. But my brain is fried.
    qs = Enrollment.objects.values('course').annotate(count=Count('user')).order_by('-count')
    for entry in qs:
        entry['course'] = Course.objects.get(id=entry['course'])
    return render(request, 'registrar/unassigned-students-by-course.html', {'courses': qs})

def availability_by_time(request, **kwargs):
    # XXX: This should probably go into the model or a manager...
    # XXX: ... and it should be able to be optimized
    course = Course.objects.get(id=kwargs['pk'])
    bq = Enrollment.objects.filter(course=course)
    data = [[day[1] for day in DAY_CHOICES]]
    for time in TIME_CHOICES:
        line = [bq.filter(preferred_days__contains=day[0]).filter(preferred_time__contains=time[0]).count() for day in DAY_CHOICES]
        line.insert(0, time[1])
        data.append(line)
    print("DATA", data)
    return render(request, 'registrar/availability-by-time.html', {'data': data, 'course': course})
