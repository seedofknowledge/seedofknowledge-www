from django.urls import path
from .views import *

urlpatterns = [
    path('users/', UserList.as_view(), name='user-list'),
    path('users/<int:pk>/', UserDetail.as_view(), name='user-detail'),
    path('users/<int:pk>/make_course_manager/', make_course_manager, name='make-course-manager'),
    path('users/<int:pk>/revoke_course_manager/', revoke_course_manager, name='revoke-course-manager'),
    path('users/<int:pk>/make_registrar/', make_registrar, name='make-registrar'),
    path('users/<int:pk>/revoke_registrar/', revoke_registrar, name='revoke-registrar'),

    path('schools/', SchoolList.as_view(), name='school-list'),
    path('schools/create/', SchoolCreate.as_view(), name='school-create'),

    path('schools/<int:pk>/', SchoolDetail.as_view(), name='school-detail'),
    path('schools/<int:pk>/add_classroom/', ClassroomCreate.as_view(), name='classroom-create'),
    path('teachers/', TeacherList.as_view(), name='teacher-list'),
    path('teachers/create/', TeacherCreate.as_view(), name='teacher-create'),
]
