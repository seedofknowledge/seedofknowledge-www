from django.urls import path
from .views import *

urlpatterns = [
    path('groups/', StudyGroupList.as_view(), name='study-group-list'),
    path('enrollment/<int:pk>/', EnrollmentCreate.as_view(), name='enrollment-create'),
    path('enrollment/success/<int:pk>/', enrollment_success, name='enrollment-success'),
]
