from datetime import datetime
from django.db import models
from course.models import Course
from school.models import Classroom
from core.models import Profile
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from teacher.models import Teacher

DAY_CHOICES = (
    ('MON', 'Monday'),
    ('TUE', 'Tuesday'),
    ('WED', 'Wednesday'),
    ('THU', 'Thursday'),
    ('FRI', 'Friday'),
    ('SAT', 'Saturday'),
    ('SUN', 'Sunday')
)

TIME_CHOICES = (
    (8, '8am'),
    (10, '10am'),
    (12, '12pm'),
    (14, '2pm'),
    (16, '4pm'),
    (18, '6pm'),
    (20, '8pm'),
)

class StudyGroup(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE)
    started = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)
    teacher = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return "{} {}".format(self.course.title, self.course.level)

    def is_full(self):
        return self.classroom.capacity <= Enrollment.objects.filter(study_group=self).count()

    def save(self, *args, **kwargs):
        qs = Teacher.objects.filter(user=self.teacher, course=self.course)
        if qs.count() != 1:
            raise ValidationError("Not a teacher of that course")

class Schedule(models.Model):
    study_group = models.ForeignKey(StudyGroup, related_name='schedule', on_delete=models.CASCADE)
    lesson_number = models.IntegerField()
    lesson_date = models.DateField()
    lesson_start = models.TimeField()
    lesson_end = models.TimeField()
    completed = models.BooleanField(default=False)

    def __str__(self):
        return "{}, Lesson {}: {}, {} - {}".format(self.study_group.course, self.lesson_number, self.lesson_date, self.lesson_start, self.lesson_end)

class Enrollment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    preferred_days = models.CharField(max_length=80)
    preferred_time = models.CharField(max_length=80)
    comment = models.TextField()
    timestamp = models.DateTimeField(default=datetime.now)
    enrolled = models.BooleanField(default=False)
    enrolled_timestamp = models.DateTimeField(null=True)

    def __str__(self):
        return "{}: {}".format(self.user, self.course)

    class Meta:
        unique_together = ('user', 'course')
