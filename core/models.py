from django.db import models
from django.db.models import Sum
from django.db.models.signals import post_save
from django.conf import settings
from django.dispatch import receiver

# Create your models here.

class ScorecardActivity(models.Model):
    name = models.CharField(max_length=60)

    def __str__(self):
        return self.name

class Scorecard(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, 
            on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now_add=True)
    activity = models.ForeignKey('ScoreCardActivity', on_delete=models.CASCADE)
    description = models.TextField()
    points = models.IntegerField()

    def __str__(self):
        # XXX: I'll figure out later what to do with this.
        return self.user.username + " " + self.points

class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
            on_delete=models.CASCADE)
    course_manager = models.BooleanField(default=False)
    registrar = models.BooleanField(default=False)

    @receiver(post_save, sender=settings.AUTH_USER_MODEL)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=settings.AUTH_USER_MODEL)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    def total_score(self):
        a = Scorecard.objects.filter(user=self.user).aggregate(Sum('points'))
        return a['points__sum'] or 0
