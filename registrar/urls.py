from django.urls import path
from .views import *

urlpatterns = [
    path('studygroup/add', StudyGroupCreate.as_view(), name='study-group-add'),
    path('reports/unassigned-students-by-course', unassigned_students_by_course, name='unassigned-students-by-course'),
    path('reports/availability-by-time/<int:pk>/', availability_by_time, name='availability-by-time'),
    #path('studygroup/<int:pk>', StudyGroupDetail.as_view(), name='study-group-detail'),
]
