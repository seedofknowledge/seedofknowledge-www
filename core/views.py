from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import RegistrationForm

# Create your views here.

def frontpage(request):
    if request.user.is_authenticated:
        return redirect('dashboard')
    else:
        return render(request, 'core/frontpage.html', {})

@login_required
def dashboard(request):
    return render(request, 'core/dashboard.html', {})
        
def register(request):
    if request.user.is_authenticated:
        return redirect('dashboard')
    elif request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = RegistrationForm()
    return render(request, 'core/register.html', {'form': form})


