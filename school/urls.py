from django.urls import path
from .views import *

urlpatterns = [
    path('users/', UserList.as_view(), name='user-list'),
    path('users/<int:pk>/', UserDetail.as_view(), name='user-detail'),
    path('users/<int:pk>/make_course_manager', make_course_manager, name='make-course-manager'),
    path('users/<int:pk>/revoke_course_manager', revoke_course_manager, name='revoke-course-manager'),
    path('users/<int:pk>/make_registrar', make_registrar, name='make-registrar'),
    path('users/<int:pk>/revoke_registrar', revoke_registrar, name='revoke-registrar'),
]
