from django import forms
from django.forms import ValidationError
import django.core.exceptions
from django.forms import widgets
from django.contrib.auth.models import User
import datetime
from students.models import StudyGroup, Schedule
from course.models import Course
from school.models import Classroom
from teacher.models import Teacher
from .choices import TIME_CHOICES

class DateInput(forms.DateInput):
    input_type = 'date'

class StudyGroupForm(forms.Form):
    course = forms.ModelChoiceField(queryset=Course.objects.all())
    classroom = forms.ModelChoiceField(queryset=Classroom.objects.all())
    teacher = forms.ModelChoiceField(queryset=User.objects.all())
    comment = forms.CharField(widget=forms.Textarea, required=False)
    day = forms.DateField(widget=DateInput())
    start_time = forms.ChoiceField(choices=TIME_CHOICES)

    def save(self):
        print(self.cleaned_data)
        group = StudyGroup()
        group.course = self.cleaned_data['course']
        group.classroom = self.cleaned_data['classroom']
        group.teacher = self.cleaned_data['teacher']
        group.comment = self.cleaned_data['comment']
        group.save()

        day = self.cleaned_data['day']
        start_time = datetime.time(hour=int(self.cleaned_data['start_time']))
        end_time = datetime.time(hour=int(self.cleaned_data['start_time']) + 1, minute=45)

        for lesson in range(group.course.lesson_count):
            schedule = Schedule()
            schedule.study_group = group
            schedule.lesson_number = lesson + 1
            schedule.lesson_date = day
            schedule.lesson_start = start_time
            schedule.lesson_end = end_time
            schedule.save()
            day = day + datetime.timedelta(days=7)

    def clean_teacher(self):
        teacher = self.cleaned_data['teacher']
        course = self.cleaned_data['course']
        qs = Teacher.objects.filter(user=teacher, course=course)
        if qs.count != 1:
            raise ValidationError("Not a teacher of that course!")

    class Meta:
        pass
        #model = StudyGroup
        #fields = ('course', 'classroom', 'teacher', 'comment')


    #def save(self):
    #    print("SAVE")
    #    group = super().save(commit=False)
    #    #group.start_time = datetime.time(hour=group.start_time)
    #    #group.end_time = datetime.time(hour=group.start_time + 1, minute=45)
    #    group.save()
    #    for lesson in range(group.course.lesson_count):
    #        schedule = Schedule(lesson_number=lesson+1, lesson_date=self.day, lesson_start=self.start_time, lesson_end=self.end_time)
    #        schedule.save()
    #    return group
