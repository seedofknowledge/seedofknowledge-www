from django.test import TestCase
#from django.db.utils import IntegrityError
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.contrib.auth.models import User
from course.models import CourseCategory, Course

class CourseCategoryTestCase(TestCase):
    def setUp(self):
        User.objects.create(username="tester", email="tester@testing.com", password="testS3kret.")
        CourseCategory.objects.create(name="Parent")
        
    def test_emtpy_name(self):
        """ Empty category name should not be allowed. """
        self.assertRaises(ValidationError, CourseCategory.objects.create)

    def test_duplicate_categories(self):
        """ Categories should be unique. """
        parent = CourseCategory.objects.get(name='Parent')
        CourseCategory.objects.create(name='Child', parent=parent)
        self.assertRaises(ValidationError, CourseCategory.objects.create, name='Child', parent=parent)

class CourseTestCase(TestCase):
    def setUp(self):
        User.objects.create(username="course_tester", email='course_tester@testing.com', password='tests3kr3t')
        CourseCategory.objects.create(name="Course Test Category")
        self.category = CourseCategory.objects.get(name='Course Test Category')

    def test_empty_name(self):
        """ Empty course title should not be allowed. """
        self.assertRaises(ValidationError, Course.objects.create, title='', level=1, lesson_count=10, category=self.category)

    def test_null_name(self):
        """ Null course title should not be allowed. """
        self.assertRaises(ValidationError, Course.objects.create, title=None, level=1, lesson_count=10, category=self.category)

    def test_no_level(self):
        """ A course should have a level. """
        self.assertRaises(IntegrityError, Course.objects.create, title='Course Test Course', lesson_count=10, category=self.category)

    def test_level_less_than_1(self):
        """ Course level must be >= 1. """
        self.assertRaises(ValidationError, Course.objects.create, title="Course Test Course", lesson_count=10, category=self.category, level=0)
