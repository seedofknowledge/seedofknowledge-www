from django.shortcuts import render
from django.views.generic import ListView
from django.contrib.auth.mixins import UserPassesTestMixin
from course.models import *

class CourseList(UserPassesTestMixin, ListView):
    model = Course
    context_object_name = 'courses'
    template_name = 'coursemanager/course_list.html'

    def test_func(self):
        return self.request.user.profile.course_manager
