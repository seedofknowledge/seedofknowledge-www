from django.urls import path
from .views import *

urlpatterns = [
    path('application/create/', ApplicationCreate.as_view(), name='application-new'),
]
