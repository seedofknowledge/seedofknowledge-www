from django.test import TestCase, Client
from django.db.utils import IntegrityError
from django.contrib.auth.models import User
from course.models import Course, CourseCategory
from students.models import Enrollment

class EnrollmentTestCase(TestCase):
    def setUp(self):
        User.objects.create(username='nii', email='nii@la.net', password='t0ps3kr3tpa55w0rd')
        User.objects.create(username='impostor', email='fake@user.net', password='somes3kret')
        CourseCategory.objects.create(name='Programming', description='foo')
        category = CourseCategory.objects.get(name='Programming')
        Course.objects.create(title='Python', level=1, category=category, short_description='foo', description='bar', lesson_count=5, active=True)
        Course.objects.create(title='Java', level=1, category=category, short_description='foo', description='bar', lesson_count=10, active=True)

    def test_duplicate_enrollment(self):
        """ Enrollment model should not allow user taking course duplicates. """
        user = User.objects.get(username='nii')
        course = Course.objects.get(title='Python')
        Enrollment.objects.create(user=user, course=course)
        self.assertRaises(IntegrityError, Enrollment.objects.create, user=user, course=course)

    def test_accessing_other_users_enrollment_message(self):
        """ A user should only be able to see their own enrollment messages. """
        user = User.objects.get(username='nii')
        course = Course.objects.get(title='Java')
        Enrollment.objects.create(user=user, course=course)
        enrollment = Enrollment.objects.get(user=user, course=course)
        client = Client()
        #response = client.post('/login/', {'username': 'impostor', 'password': 'somes3kret'})
        client.login(username='impostor', password='somes3kret')
        response = client.get('/students/enrollment/success/{}/'.format(enrollment.pk))
        self.assertEqual(response.status_code, 403)

