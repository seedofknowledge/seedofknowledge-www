from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import User
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.views.generic import ListView, CreateView
from django.urls import reverse_lazy
from course.models import Course
from notifications.signals import notify
from .models import *
from .forms import *

class EnrollmentCreate(LoginRequiredMixin, CreateView):
    model = Enrollment
    form_class = EnrollmentForm
    #success_url = reverse_lazy('enrollment_success', self.object.pk)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['course'] = Course.objects.get(id=self.kwargs['pk'])
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        kwargs.update({'course': Course.objects.get(id=self.kwargs['pk'])})
        return kwargs

    def form_valid(self, form):
        users = User.objects.filter(profile__registrar=True)
        self.object = form.save()
        notify.send(self.request.user, recipient=users, verb="incoming application") 
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('enrollment-success', args=[self.object.pk])

def enrollment_success(request, *args, **kwargs):
    enrollment = Enrollment.objects.get(pk=kwargs['pk'])
    if not enrollment.user == request.user:
        raise PermissionDenied()
    return render(request, 'students/enrollment_success.html', {'enrollment': enrollment })

class StudyGroupList(UserPassesTestMixin, ListView):
    model = StudyGroup
    context_object_name = 'study_groups'

    def test_func(self):
        return self.request.user.profile.registrar


