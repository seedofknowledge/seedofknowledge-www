from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import ListView, CreateView, DetailView
from django.urls import reverse_lazy
from .models import CourseCategory, Course


class CourseCategoryList(UserPassesTestMixin, ListView):
    model = CourseCategory
    context_object_name = 'categories'

    def test_func(self):
        return self.request.user.profile.course_manager

class CourseCategoryCreate(UserPassesTestMixin, CreateView):
    model = CourseCategory
    fields = ['name', 'parent', 'description']
    context_object_name = 'category'
    success_url = reverse_lazy('course-category-list')

    def test_func(self):
        return self.request.user.profile.course_manager

class CourseCatalog(LoginRequiredMixin, ListView):
    model = CourseCategory
    context_object_name = 'course_categories'
    template_name = 'course/course_catalog.html'

class CourseDetail(LoginRequiredMixin, DetailView):
    model = Course
    context_object_name = 'course'

class CourseCreate(UserPassesTestMixin, CreateView):
    model = Course
    fields = ['title', 'level', 'category', 'short_description', 'description', 'lesson_count', 'active', 'price']
    context_object_name = 'course'
    success_url = reverse_lazy('coursemanager:course-list')

    def test_func(self):
        return self.request.user.profile.course_manager

