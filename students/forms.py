from django import forms
from django.forms.widgets import CheckboxSelectMultiple
from .models import Enrollment, DAY_CHOICES, TIME_CHOICES
from course.models import Course

class EnrollmentForm(forms.ModelForm):
    class Meta:
        model = Enrollment
        fields = ('preferred_days', 'preferred_time', 'comment')

    preferred_days = forms.MultipleChoiceField(
            widget=CheckboxSelectMultiple,
            choices=DAY_CHOICES
    )
    preferred_time = forms.MultipleChoiceField(
            widget=CheckboxSelectMultiple,
            choices=TIME_CHOICES
    )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.course = kwargs.pop('course', None)
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        if Enrollment.objects.filter(user=self.user, course=self.course).count() > 0:
            raise forms.ValidationError("You cannot enroll to the same course more than once")

    def save(self):
        enrollment = super().save(commit=False)
        enrollment.user = self.user
        enrollment.course = self.course
        enrollment.preferred_days = enrollment.preferred_days[1:-1].replace("'", "").replace(" ", "")
        enrollment.preferred_time = enrollment.preferred_time[1:-1].replace("'", "").replace(" ", "")
        #application.preferred_days = ', '.join(application.preferred_days)
        #application.preferred_time = ', '.join(application.preferred_time)
        enrollment.save()
        return enrollment


