from django.db import models

class School(models.Model):
    name = models.CharField(max_length=100, unique=True)
    location = models.TextField()
    city = models.CharField(max_length=50)

    def __str__(self):
        return "{}, {}".format(self.name, self.city)

class Classroom(models.Model):
    label = models.CharField(max_length=100)
    school = models.ForeignKey(School, related_name='classrooms', on_delete=models.CASCADE)
    capacity = models.IntegerField()

    def __str__(self):
        return "{}, {}".format(self.label, self.school)
