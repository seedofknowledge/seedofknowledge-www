# seedofknowledge-www

*Note:* Kindly help me to keep this documentation up-to-date and add instructions for other operating systems.

## Setting up for local development


### Step 1: One time setup


#### Step 1.1: OS level dependencies

First you need to satisfy the requirements.

1. Python 3.6+, with pip
2. Postgresql
3. Git

On Fedora,

    # dnf install python3 python3-pip postgresql-server git

#### Step 1.2: pull down the seedofknowledge-www sources

    % cd <your development directory>
    % git clone git@bitbucket.org:seedofknowledge/seedofknowledge-www.git 

#### Step 1.3: create the virtual environment

    % cd seedofknowledge-www
    % python3 -m venv .venv
    % source .venv/bin/activate
    (.venv) % pip install -r requirements.txt
    
#### Step 1.4: set up postgresql

*NB:* IIRC, there is some initialization stuff to do before starting Postgres the first time. Please help me add it here.
    
    # postgresql-setup initdb
    # systemctl start postgresql
    # su - postgres
    $ vim data/pg_hba.conf

In this file, look for the line:

    host    all    all    127.0.0.1/32    ident

And change the ident to md5. Save the file and exit vim.

    $ createuser --interactive --pwprompt

* When it asks for user (role) name: *sok*
* When it asks for password: *fn00fnarfnunG!!*
* When it asks if the user should be a super user: *N*
* When it asks if the user should create databases: *Y*
* When it asks if the user should create roles: *N*

    $ exit
    # systemctl restart postgresql
    (.venv) % psql --host localhost --user sok template1
    [TYPE PASSWORD WHEN PROMPTED]

This will test if your user creation was successful. You should also go ahead and create the database.

    template1=> create database seedofknowledge;
    template1=> \q
    (.venv) % 

#### Step 1.5: perform migrations

    (.venv) % ./manage.py migrate

Hopefully all goes well.

#### Step 1.6: create a superuser account

    (.venv) % ./manage.py createsuperuser
    [FOLLOW THE PROMPTS]

### Step 2: Running seedofknowledge

    (.venv) % ./manage.py runserver

Point your browser to http://localhost:8000. You should be able to log in with the superuser account you just created.

### Step 3: running the tests

Run the tests with:

   (.venv) % ./manage.py test

Study the output of the tests well. No tests should fail. If they do, this must be reported as a bug.

### Step 4: things to do regularly

After a git pull, you should:

    % ./manage.py migrate

To perform any database migrations that are needed.

    % ./manage.py test

To ensure that all the tests run successfully.





