from django.urls import path
from .views import *

app_name = 'coursemanager'
urlpatterns = [
    path('courses/', CourseList.as_view(), name='course-list'),
]
