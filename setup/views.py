from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import UserPassesTestMixin
from django.views.generic import ListView, DetailView, CreateView
from django.urls import reverse_lazy
from school.models import School, Classroom
from school.forms import ClassroomForm
from teacher.models import Teacher

class UserList(UserPassesTestMixin, ListView):
    model = User
    context_object_name = 'members'
    template_name = 'setup/user_list.html'

    def test_func(self):
        return self.request.user.is_superuser

class UserDetail(UserPassesTestMixin, DetailView):
    model = User
    context_object_name = 'member'
    template_name = 'setup/user_detail.html'

    def test_func(self):
        return self.request.user.is_superuser

@user_passes_test(lambda u: u.is_superuser)
def make_course_manager(request, **kwargs):
    user = User.objects.get(id=kwargs['pk'])
    user.profile.course_manager = True
    user.profile.save()
    return redirect('user-list')

@user_passes_test(lambda u: u.is_superuser)
def revoke_course_manager(request, **kwargs):
    user = User.objects.get(id=kwargs['pk'])
    user.profile.course_manager = False
    user.profile.save()
    return redirect('user-list')

@user_passes_test(lambda u: u.is_superuser)
def make_registrar(request, **kwargs):
    user = User.objects.get(id=kwargs['pk'])
    user.profile.registrar = True
    user.profile.save()
    return redirect('user-list')

@user_passes_test(lambda u: u.is_superuser)
def revoke_registrar(request, **kwargs):
    user = User.objects.get(id=kwargs['pk'])
    user.profile.registrar = False
    user.profile.save()
    return redirect('user-list')

class SchoolList(UserPassesTestMixin, ListView):
    model = School
    context_object_name = 'schools'
    template_name = 'setup/school_list.html'

    def test_func(self):
        return self.request.user.is_superuser

class SchoolCreate(UserPassesTestMixin, CreateView):
    model = School
    fields = ['name', 'location', 'city']
    template_name = 'setup/school_form.html'
    success_url = reverse_lazy('school-list')

    def test_func(self):
        return self.request.user.is_superuser

class SchoolDetail(UserPassesTestMixin, DetailView):
    model = School
    context_object_name = 'school'
    template_name = 'setup/school_detail.html'

    def test_func(self):
        return self.request.user.is_superuser

class ClassroomCreate(UserPassesTestMixin, CreateView):
    model = Classroom
    form_class = ClassroomForm
    template_name = 'setup/classroom_form.html'

    def test_func(self):
        return self.request.user.is_superuser

    def get_initial(self):
        initial = super().get_initial()
        initial['school'] = self.kwargs['pk']
        return initial

    def get_success_url(self):
        return reverse_lazy('school-detail', kwargs=self.kwargs)

class TeacherList(UserPassesTestMixin, ListView):
    model = Teacher
    context_object_name = 'teachers'
    template_name = 'setup/teacher_list.html'

    def test_func(self):
        return self.request.user.is_superuser

class TeacherCreate(UserPassesTestMixin, CreateView):
    model = Teacher
    fields = ['user', 'course']
    template_name = 'setup/teacher_form.html'
    success_url = reverse_lazy('teacher-list')

    def test_func(self):
        return self.request.user.is_superuser
