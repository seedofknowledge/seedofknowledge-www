# Generated by Django 2.1.1 on 2018-10-05 09:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0006_course_short_description'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='coursecategory',
            unique_together={('name', 'parent')},
        ),
    ]
