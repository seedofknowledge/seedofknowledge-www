from django.urls import path
from .views import *

urlpatterns = [
    path('categories/', CourseCategoryList.as_view(), name='course-category-list'),
    path('categories/add/', CourseCategoryCreate.as_view(), name='course-category-add'),
    path('', CourseCatalog.as_view(), name='course-catalog'),
    path('<int:pk>/', CourseDetail.as_view(), name='course-detail'),
    path('add', CourseCreate.as_view(), name='course-add'),
]
